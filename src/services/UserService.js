import axios from 'axios'

import handleApiError from './handleApiError'
import axiosHeaders from './axiosHeaders'

axiosHeaders();

const BASE_URL = 'http://aportal.tvcompany.com:80/stalker_portal';

export default {
    async user(id) {
        return await axios.get(`${BASE_URL}/api/v2/users/`+id).catch(handleApiError);
    },
    async claim(id, claim) {
        var qs = require('qs');
        return await axios.post('http://api.portal.tvcompany.com/claim/'+id, qs.stringify(claim), { 'headers': { 'Accept-Language': 'ru-RU', 'X-Request-Api': 'token' }} ).catch(handleApiError);
    },
    async statisticTV(params) {
        var qs = require('qs');
        return await axios.post('http://api.portal.tvcompany.com/viewtv/', qs.stringify(params), { 'headers': { 'Accept-Language': 'ru-RU', 'X-Request-Api': 'token','Connection': 'keep-alive', 'Content-Length': '30' }} ).catch(handleApiError);
    },
    async statisticTimeshift(params) {
        var qs = require('qs');
        return await axios.post('http://api.portal.tvcompany.com/viewtimeshift/', qs.stringify(params), { 'headers': { 'Accept-Language': 'ru-RU', 'X-Request-Api': 'token','Connection': 'keep-alive', 'Content-Length': '30' }} ).catch(handleApiError);
    },
    async statisticArchive(params) {
        var qs = require('qs');
        return await axios.post('http://api.portal.tvcompany.com/viewarchive/', qs.stringify(params), { 'headers': { 'Accept-Language': 'ru-RU', 'X-Request-Api': 'token','Connection': 'keep-alive', 'Content-Length': '30' }} ).catch(handleApiError);
    },
    async statisticVideo(params) {
        var qs = require('qs');
        return await axios.post('http://api.portal.tvcompany.com/viewvideo/', qs.stringify(params), { 'headers': { 'Accept-Language': 'ru-RU', 'X-Request-Api': 'token','Connection': 'keep-alive', 'Content-Length': '30' }} ).catch(handleApiError);
    },
}
