export default function (err) {

    err.validation = {};
    err.msg = null;

    if('response' in err) {

        // if(err.response.data) {
        //     err.msg = err.response.data.message || err.message;
        // }
        // if(err.response.status === 403) {
        //     window.location.reload();
        // } else if(err.response.status === 422) {
        //     err.validation = err.response.data;
        // }
        if (err.response.data.error === 'Authorization required' || err.response.data.error === 'Access token wrong or expired') {
            console.log('err resp', err.response.data.error);
            window.location='/logout';
        }



    } else {
        console.error(err); // eslint-disable-line no-use-before-define
    }


    return {data: null, err: err};
}
