import axios from 'axios'

import handleApiError from './handleApiError'
import axiosHeaders from './axiosHeaders'

axiosHeaders();

const BASE_URL = 'http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video/';
const BASE_URL_LIST = 'http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video-categories/';
const BASE_URL_FAVORITE = 'http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video-favorites/';
const BASE_URL_CLEAR = 'http://aportal.tvcompany.com:80/stalker_portal/api/v2/video-categories/';

export default {
    async list(type) {
        type = type ? type : 'movies';
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video-categories/'+type+'/video?sortby_desc=​added').catch(handleApiError);
    },
    async listAwait(order) {
        order = order ? order : 0;
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video-categories/movies/video?offset='+order+'&limit=100&sortby_desc=​added').catch(handleApiError);
    },
    async listLast() {
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video-categories/movies/video?limit=20&sortby_desc=​added').catch(handleApiError);
    },
    async listLength() {
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video-categories/movies/video/count').catch(handleApiError);
    },
    async listTop() {
        return await axios.get('http://api.portal.tvcompany.com/top/', { 'headers': { 'Accept-Language': 'ru-RU', 'X-Request-Api': 'token' }}).catch(handleApiError);
    },
    async listFavorite() {
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video?mark=favorite').catch(handleApiError);
    },
    async listTopMonth() {
        return await axios.get('http://api.portal.tvcompany.com/topMonth/', { 'headers': { 'Accept-Language': 'ru-RU', 'X-Request-Api': 'token' }}).catch(handleApiError);
    },
    async listTopToday() {
        return await axios.get('http://api.portal.tvcompany.com/watchToday/', { 'headers': { 'Accept-Language': 'ru-RU', 'X-Request-Api': 'token' }}).catch(handleApiError);
    },
    async view(id) {
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video/'+id).catch(handleApiError);
    },
    async viewMore(id) {
        return await axios.get('http://api.portal.tvcompany.com/video/'+id, { 'headers': { 'Accept-Language': 'ru-RU', 'X-Request-Api': 'token' }} ).catch(handleApiError);
    },
    async viewQuality(id) {
        return await axios.get('http://api.portal.tvcompany.com/quality/'+id, { 'headers': { 'Accept-Language': 'ru-RU', 'X-Request-Api': 'token' }} ).catch(handleApiError);
    },
    async viewFrames(id) {
        return await axios.get('http://api.portal.tvcompany.com/frames/'+id, { 'headers': { 'Accept-Language': 'ru-RU', 'X-Request-Api': 'token' }} ).catch(handleApiError);
    },
    async link(id) {
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video/'+id+'/link').catch(handleApiError);
    },
    async genres() {
        return await axios.get(`${BASE_URL_CLEAR}`+'/movies/video-genres').catch(handleApiError);
    },

    async viewVotes(id) {
        let qs = require('qs');
        return await axios.post('http://api.portal.tvcompany.com/viewvote/', qs.stringify({'id':id,'uid':localStorage.getItem('user')}), { 'headers': { 'Accept-Language': 'ru-RU', 'X-Request-Api': 'token' }} ).catch(handleApiError);
    },
    async likeVotes(id, like) {
        let qs = require('qs');
        return await axios.post('http://api.portal.tvcompany.com/vote/', qs.stringify({'id':id,'likes':like,'uid':localStorage.getItem('user')}), { 'headers': { 'Accept-Language': 'ru-RU', 'X-Request-Api': 'token' }} ).catch(handleApiError);
    },

    async notEnded(id, end_time) {
        let qs = require('qs');
        return await axios.put('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video/'+id+'/not-ended', qs.stringify({end_time:end_time})).catch(handleApiError);
    },
    async notEndedSeries(id, end_time, episode) {
        let qs = require('qs');
        return await axios.put('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video/'+id+'/not-ended', qs.stringify({end_time:end_time, episode: episode})).catch(handleApiError);
    },
    async getNotEndedTime(id) {
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video/'+id+'/not-ended').catch(handleApiError);
    },
    async destroyNotEndedTime(id) {
        return await axios.delete('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video/'+id+'/not-ended').catch(handleApiError);
    },


    async addFavorite(id) {
        let qs = require('qs');
        return await axios.post('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video-favorites/', qs.stringify({'video_id':id}) ).catch(handleApiError);
    },
    async removeFavorite(id) {
        return await axios.delete('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video-favorites/'+id).catch(handleApiError);
    },


    async episodesLink(id, episode) {
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video/'+id+'/episodes/'+episode+'/link').catch(handleApiError);
    },
    async seasons(id) {
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/video/'+id+'/seasons/').catch(handleApiError);
    },

}
