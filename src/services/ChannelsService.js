import axios from 'axios'

import handleApiError from './handleApiError'
import axiosHeaders from './axiosHeaders'

axiosHeaders();

const BASE_URL = 'http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/tv-channels/';
const BASE_URL_FAVORITE = 'http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/tv-favorites/';
const BASE_URL_GENRES = 'http://aportal.tvcompany.com:80/stalker_portal/api/v2/tv-genres/';


export default {
    async list() {
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/tv-channels/').catch(handleApiError);
    },
    async listFavorite() {
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/tv-channels?mark=favorite').catch(handleApiError);
    },
    async listTop() {
        return await axios.get('http://api.portal.tvcompany.com/toptv/', { 'headers': { 'Accept-Language': 'ru-RU', 'X-Request-Api': 'token' }}).catch(handleApiError);
    },
    async view(id) {
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/tv-channels/'+id).catch(handleApiError);
    },
    async link(id) {
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/tv-channels/'+id+'/link').catch(handleApiError);
    },
    async timeshift(id, start) {
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/tv-channels/'+id+'/link?start='+start).catch(handleApiError);
    },
    async epg(id) {
        let currentDate = new Date(),
            date = currentDate.getDate(),
            month = currentDate.getMonth(),
            year = currentDate.getFullYear(),
            abs = 3600 * 24,
            today = new Date(year, month, date, 0, 0 ,0),
            start = today.getTime()/1000 - abs,
            end = today.getTime()/1000 + abs + abs - 1;
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/tv-channels/'+id+'/epg?from='+start+'&to='+end).catch(handleApiError);
    },
    async epgLink(id) {
        return await axios.get('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/tv-channels/'+'/epg/'+id+'/link').catch(handleApiError);
    },

    async addFavorite(id) {
        let qs = require('qs');
        return await axios.post('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/tv-favorites/', qs.stringify({'ch_id':id}) ).catch(handleApiError);
    },
    async removeFavorite(id) {
        return await axios.delete('http://aportal.tvcompany.com:80/stalker_portal/api/v2/users/'+localStorage.getItem('user')+'/tv-favorites/'+id).catch(handleApiError);
    },

    async genres() {
        return await axios.get(`${BASE_URL_GENRES}`).catch(handleApiError);
    },


}
