import axios from 'axios'

import handleApiError from './handleApiError'

const BASE_URL = 'http://aportal.tvcompany.com:80/stalker_portal/auth/token';

export default {

    async login(grant_type, username, password) {
        axios.defaults.headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept-Language': 'ru-RU',
            'Accept': 'application/json'
        };

        var qs = require('qs');
        return await axios.post(`http://aportal.tvcompany.com:80/stalker_portal/auth/token`, qs.stringify({grant_type, username, password}) ).catch(handleApiError);
    },
}
