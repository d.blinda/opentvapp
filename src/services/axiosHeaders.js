import axios from "axios";

export default function () {
    if (localStorage.getItem('token') && localStorage.getItem('token').length <= 4) {
        axios.defaults.headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept-Language': 'ru-RU',
            'Accept': 'application/json'
        };
    } else {
        axios.defaults.headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Accept-Language': 'ru-RU',
            'Pragma': 'no-cache',
            'Expires': 0,
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
        };
    }
}
