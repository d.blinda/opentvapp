import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'

import VueVideoPlayer from 'vue-video-player'
import 'video.js/dist/video-js.css'


import videojs from 'video.js'
import thumbnails from "./plugins/videojs.thumbnails";
const Plugin = videojs.getPlugin("plugin");
videojs.registerPlugin("thumbnails", thumbnails);
import './plugins/vjs-styles.css'


import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
Vue.use(VueAwesomeSwiper);

Vue.use(VueVideoPlayer, /* {
  options: global default options,
  events: global videojs events
} */);

import videojshotkeys from 'videojs-hotkeys'
Vue.use(videojshotkeys);

let SocialSharing = require('vue-social-sharing');
Vue.use(SocialSharing);

import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret, faSpinner, faAlignLeft, faSort, faSortAmountDown, faSortAmountUp} from '@fortawesome/free-solid-svg-icons'
import { faFacebook, faTwitter, faTelegram, faWhatsapp, faSkype, faOdnoklassniki, faVk } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(faUserSecret, faSpinner, faAlignLeft, faSort, faSortAmountDown, faSortAmountUp, faFacebook, faTwitter, faTelegram, faWhatsapp, faSkype, faOdnoklassniki, faVk);
Vue.component('fa', FontAwesomeIcon);

import VueDisqus from 'vue-disqus'
Vue.use(VueDisqus);

Vue.config.productionTip = false;

window.axios = axios;
const moment = require('moment');
require('moment/locale/es');
Vue.use(require('vue-moment'), {
  moment
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')

export {  router, store };
