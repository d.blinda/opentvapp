import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'

import LoginService from './services/LoginService';
import UserService from './services/UserService';
import ChannelsService from './services/ChannelsService';

import createLogger from 'vuex/dist/logger'
import axios from "axios";
import FilmsService from "./services/FilmsService";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    strict: debug,
    plugins: debug ? [createLogger()] : [],
    state: {
        server: 'http://aportal.tvcompany.com:80/stalker_portal/',
        host: 'http://aportal.tvcompany.com:80',
        is_loading: [],
        User: {},
        them: 'dark',
        errorLogin: '',
        genreName: '',

        channels: [],
        topChannels: [],
        favoriteChannels: [],
        channelsGenres: [],
        epg: {},

        films: [],
        filmsLast: [],
        filmsTop: [],
        filmsFavorite: [],
        filmsTopMonth: [],
        filmsTopToday: [],
        filmsGenres: [],

        channels_ids: [],
        top_channels_ids: [],
        favorite_channels_ids: [],
        films_ids: [],
        films_last_ids: [],
        top_films_ids_all: [],
        favorite_films_ids_all: [],
        top_films_ids_today: [],
        top_films_ids_month: [],
        channelsGenres_ids: [],
        filmsGenres_ids: [],

        filmsLength: 0,
        loadAllMovies: false,
        loadAllChannels: false,

        loadAllFilmsFlag: false,

        mainPageInfoLoad: false,

        startTime: false,

        qualitys: {
            '7': '4K',
            '6': '1080',
            '5': '720',
            '4': '576',
            '3': '480',
            '2': '320',
            '1': '240'
        }

    },


    getters: {

        getChannel: state => id => state.channels[id] ? state.channels[id] : null,
        getChannelFav: state => id => state.favoriteChannels[id] ? state.favoriteChannels[id] : null,
        getChannels: (state, getters) => state.channels_ids.map(id => getters.getChannel(id)),
        getChannelsFavorite: (state, getters) => state.favorite_channels_ids.map(id => getters.getChannelFav(id)),
        getTopChannels: (state, getters) => state.top_channels_ids.map(id => getters.getChannel(id)),

        getChannelsGenre: state => id => state.channelsGenres[id] ? state.channelsGenres[id] : null,
        getChannelsGenres: (state, getters) => state.channelsGenres_ids.map(id => getters.getChannelsGenre(id)),

        isLoading: state => key => state.is_loading.indexOf(key) > -1,


        getFilm: state => id => state.films[id] ? state.films[id] : null,
        getFilmFav: state => id => state.filmsFavorite[id] ? state.filmsFavorite[id] : null,
        getFilmLast: state => id => state.filmsLast[id] ? state.filmsLast[id] : null,
        getFilms: (state, getters) => state.films_ids.map(id => getters.getFilm(id)),
        getFilmsLast: (state, getters) => state.films_last_ids.map(id => getters.getFilmLast(id)),
        getFilmsTop: (state, getters) => state.top_films_ids_all.map(id => getters.getFilm(id)),
        getFilmsFavorite: (state, getters) => state.favorite_films_ids_all.map(id => getters.getFilmFav(id)),
        getFilmsTopMonth: (state, getters) => state.top_films_ids_month.map(id => getters.getFilm(id)),
        getFilmsTopToday: (state, getters) => state.top_films_ids_today.map(id => getters.getFilm(id)),
        // getFilmsSortName: (state, getters) => state.films
        getFilmGenre: state => id => state.filmsGenres[id] ? state.filmsGenres[id] : null,
        getFilmGenres: (state, getters) => state.filmsGenres_ids.map(id => getters.getFilmGenre(id)),

        // getTransports: state => state.transports ? state.transports : null,
        // getTransportByType: state => id => state.transports[id] ? state.transports[id] : null,
        // getTransportIconByType: (state, getters) => id => state.transports[id] ? state.transports[id].icon : null,
        // getTransportColorByType: (state, getters) => id => state.transports[id] ? state.transports[id].color : null,
        //
        // getRoute: state => id => state.routes[id] ? state.routes[id] : null,
        // getRoutes: (state, getters) => state.routes_ids.map(id => getters.getRoute(id)),

    },


    mutations: {
        set (state, {type, items}) {
            state[type] = items
        },
        setTheme: (state, data) => {
            state.them = data
        },
        setStartTime: (state, data) => {
            state.startTime = data
        },
        setChannel: (state, data) => {
            Vue.set(state.channels, data.id, {...state.channels[data.id], ...data, is_loaded: true});
        },
        setChannels: (state, data) => {
            if (data) {
                data.forEach(item => Vue.set(state.channels, item.id, {...item, is_loaded: false, link: null, epg: null}));
                state.channels_ids = data.map(r => r.id);
            }
        },
        setTopChannels: (state, data) => {
            if (data) {
                data.forEach(item => Vue.set(state.topChannels, item.id, {...item, is_loaded: false, link: null, epg: null}));
                state.top_channels_ids = data.map(r => r.id);
            }
        },
        setFavoriteChannels: (state, data) => {
            if (data) {
                data.forEach(item => Vue.set(state.favoriteChannels, item.id, {...item, is_loaded: false, link: null, epg: null}));
                state.favorite_channels_ids = data.map(r => r.id);
            }
        },
        setChannelLink: (state, {data, id}) => {
            Vue.set(state.channels, id, {...state.channels[id], link: data});
        },
        setChannelEPG: (state, {data, id}) => {
            Vue.set(state.channels, id, {...state.channels[id], epg: data});
        },
        setEPGLink: (state, data) => {
            state.epg.link = data;
        },
        setFavoriteChannel: (state, {data, id}) => {
            Vue.set(state.channels, id, {...state.channels[id], favorite: data});
        },
        setChannelsGenres: (state, data) => {
            data.forEach(item => Vue.set(state.channelsGenres, item.id, item));
            state.channelsGenres_ids = data.map(r => r.id);
        },


        setFilms: (state, data) => {
            if (data) {
                console.log('wait');
                data.forEach(item => Vue.set(state.films, item.id, {...item, is_loaded: false, link: null}));
                state.films_ids = data.map(r => r.id);
            }
        },
        setFilmsAwait: (state, data) => {
            if (data) {
                data.forEach(item => {
                    if (!!state.films[item.id]) {
                        console.log('setFilmsAwaitId',item.id);
                    } else {
                        Vue.set(state.films, item.id, {...item, is_loaded: false, link: null});
                    }
                    state.films_ids.push(item.id)
                });
            }
        },
        setFilmsLast: (state, data) => {
            if (data) {
                data.forEach(item => Vue.set(state.filmsLast, item.id, {...item, is_loaded: false, link: null}));
                state.films_last_ids = data.map(r => r.id);
            }
        },
        setFilmsTop: (state, data) => {
            if (data) {
                data.forEach(item => Vue.set(state.filmsTop, item.id, {...item, is_loaded: false, link: null}));
                state.top_films_ids_all = data.map(r => r.id);
            }
        },
        setFilmsFavorite: (state, data) => {
            if (data) {
                data.forEach(item => Vue.set(state.filmsFavorite, item.id, {...item, is_loaded: false, link: null}));
                state.favorite_films_ids_all = data.map(r => r.id);
            }
        },
        setFilmsTopMonth: (state, data) => {
            if (data) {
                data.forEach(item => Vue.set(state.filmsTopMonth, item.id, {...item, is_loaded: false, link: null}));
                state.top_films_ids_month = data.map(r => r.id);
            }
        },
        setFilmsTopToday: (state, data) => {
            if (data) {
                data.forEach(item => Vue.set(state.filmsTopToday, item.id, {...item, is_loaded: false, link: null}));
                state.top_films_ids_today = data.map(r => r.id);
            }
        },
        setFilm: (state, data) => {
            Vue.set(state.films, data.id, {...state.films[data.id], ...data, is_loaded: true});
        },
        setFilmMore: (state, data) => {
            let seasons = [];
            if (data.episodes) {
                data.episodes.forEach( item => seasons.push(item.season_number) );
            }
            let unique = seasons.filter((thing, index, self) =>
                index === self.findIndex((t) => (
                    t === thing
                ))
            );
            console.log('setFilmMore',state.films[data.id]);
            Vue.set(state.films, data.id, {...state.films[data.id], more: data, seasons: unique});
        },
        setFilmQuality: (state, {data, id}) => {
            Vue.set(state.films, id, {...state.films[id], quality: data} );
        },
        setFilmFrames: (state, {data, id}) => {
            Vue.set(state.films, id, {...state.films[id], frames: data} );
        },
        setFilmVotes: (state, {data, id}) => {
            Vue.set(state.films, id, {...state.films[id], votes: data} );
        },
        setFilmEnded: (state, {data, id}) => {
            Vue.set(state.films, id, {...state.films[id], end_time: data} );
        },
        setFilmLink: (state, {data, id}) => {
            Vue.set(state.films, id, {...state.films[id], link: data});
        },
        setFilmsGenres: (state, data) => {
            data.forEach(item => Vue.set(state.filmsGenres, item.id, item));
            state.filmsGenres_ids = data.map(r => r.id);
        },
        setFavoriteFilm: (state, {data, id}) => {
            Vue.set(state.films, id, {...state.films[id], favorite: data});
        },
        setNotEnded: (state, {id, end_time}) => {
            Vue.set(state.films, id, {...state.films[id], not_ended: 1, end_time: end_time});
        },
        setNotEndedEpisode: (state, {id, end_time, episode}) => {
            Vue.set(state.films, id, {...state.films[id], not_ended: 1, not_ended_episode: episode, end_time: end_time});
        },
        destroyNotEnded: (state, id) => {
            Vue.set(state.films, id, {...state.films[id], not_ended: 0, not_ended_episode: '0'});
        },


        startLoading: (state, key) => {
            if (state.is_loading.indexOf(key) === -1) {
                state.is_loading.push(key);
            }
        },
        stopLoading: (state, key) => {
            state.is_loading = state.is_loading.filter(k => k !== key);
        },
    },


    actions: {
        async loginUser({commit}, {grant_type, username, password}) {
            commit('startLoading', 'token');
                const {data} = await LoginService.login(grant_type, username, password);
                commit('set',{type: 'user', items: data});

                if (data.access_token != null) {
                    localStorage.setItem('token', data.access_token);
                    localStorage.setItem('user', data.user_id);
                    commit('set',{type: 'errorLogin', items: ''});
                    axios.defaults.headers = {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Cache-Control': 'no-cache, no-store, must-revalidate',
                        'Accept-Language': 'ru-RU',
                        'Pragma': 'no-cache',
                        'Expires': 0,
                        'Accept': 'application/json',
                        'Authorization': 'Bearer '+ data.access_token,
                    };
                    router.push('/');
                }
                else {
                    console.log('Error Login');
                    commit('set',{type: 'errorLogin', items: 'Неверный логин или пароль!'});
                }
            commit('stopLoading', 'token');
        },
        async userInfo({ commit }) {
            const {data} = await UserService.user( localStorage.getItem('user') );
            commit('set',{type: 'User', items: data.results});

        },
        async sendClaim({ commit }, {id, claim}) {
            const {data} = await UserService.claim( id, claim );
            console.log('claim send');
        },
        async sendStatisticTV({ commit }, params) {
            const {data} = await UserService.statisticTV( params );
            console.log('Statistic view TV chanel send');
        },
        async sendStatisticTimeshift({ commit }, params) {
            const {data} = await UserService.statisticTimeshift( params );
            console.log('Statistic Timeshift TV chanel send');
        },
        async sendStatisticArchive({ commit }, params) {
            const {data} = await UserService.statisticArchive( params );
            console.log('Statistic Archive TV chanel send');
        },
        async sendStatisticVideo({ commit }, params) {
            const {data} = await UserService.statisticVideo( params );
            console.log('Statistic view Film send');
        },

        async loadInfo({commit, dispatch}) {
            commit('startLoading', 'allInfo');
                await dispatch('loadChannels');
                await dispatch('loadTopChannels');
                await dispatch('loadFilms');
                await dispatch('loadFilmsTop');
                await dispatch('loadFilmsTopMonth');
                await dispatch('loadFilmsTopToday');
                await dispatch('loadFilmsGenres');
            commit('stopLoading', 'allInfo');
        },

        async loadMainInfo({commit, dispatch}) {
            commit('startLoading', 'allMainInfo');

                await dispatch('loadFilmsGenres');
                await dispatch('loadFilmsLength');

            commit('stopLoading', 'allMainInfo');

            await dispatch('loadFilmsAwait');
        },

        async loadMainPage({commit, dispatch}) {
            commit('startLoading', 'mainPage');

                await dispatch('loadFilmsFavorite');
                await dispatch('loadFavoriteChannels');
                await dispatch('loadFilmsLast');

                commit('set',{type: 'mainPageInfoLoad', items: true});

            commit('stopLoading', 'mainPage');
        },


        async loadFilmPage({commit, dispatch}) {
            commit('startLoading', 'filmPage');

                // await dispatch('loadFilms');
                await dispatch('loadFilmsTop');
                await dispatch('loadFilmsTopMonth');
                await dispatch('loadFilmsTopToday');

            commit('stopLoading', 'filmPage');
        },

        async loadFilmAllInfo({commit, dispatch}, id) {
            commit('startLoading', 'filmView');
                await dispatch('loadFilmInfo', id);
                await dispatch('loadFilmMore', id);
                await dispatch('loadFilmQuality', id);
                await dispatch('loadFilmFrames', id);
                await dispatch('loadFilmVotes', id);
            commit('stopLoading', 'filmView');
        },




        // Channels
        async loadChannel({ commit }, id) {
            const {data} = await ChannelsService.view(id);
            commit('setChannel',data.results);
        },
        async loadChannelLink({ commit }, id) {
            const {data} = await ChannelsService.link(id);
            commit('setChannelLink',{data: data.results, id: id});
        },
        async loadTimeshiftLink({ commit }, {id, start}) {
            commit('setStartTime', Date.now() );
            const {data} = await ChannelsService.timeshift(id,start);
            commit('setChannelLink',{data: data.results, id: id});
        },
        async loadChannelEPG({ commit }, id) {
            const {data} = await ChannelsService.epg(id);
            commit('setChannelEPG',{data: data.results, id: id});
        },

        async clearStartTime({ commit }) {
            commit('setStartTime', false );
        },
        async loadChannelInfo({commit, dispatch}, id) {
            commit('startLoading', 'channel');
                await dispatch('loadChannel', id);
                await dispatch('loadChannelLink', id);
                await dispatch('loadChannelEPG', id);
            commit('stopLoading', 'channel');
        },
        async loadChannels({ commit }) {
            commit('startLoading','channels');
                const {data} = await ChannelsService.list();
                commit('setChannels',data.results);
                commit('set',{type: 'loadAllChannels', items: true});
            commit('stopLoading','channels');
        },
        async loadFavoriteChannels({ commit }) {
            commit('startLoading','channelsFavorite');
                const {data} = await ChannelsService.listFavorite();
                commit('setFavoriteChannels',data.results);
            commit('stopLoading','channelsFavorite');
        },
        async loadTopChannels({ commit }) {
            commit('startLoading','channels');
                const {data} = await ChannelsService.listTop();
                commit('setTopChannels',data.results);
            commit('stopLoading','channels');
        },

        async loadIndexChannels({commit, dispatch}) {
            commit('startLoading', 'channels');

                await dispatch('loadChannels');
                await dispatch('loadChannelsGenres');
                await dispatch('loadTopChannels');

            commit('stopLoading', 'channels');
        },


        async addFavoriteChannels({ commit,dispatch }, id) {
            const {data} = await ChannelsService.addFavorite(id);
            data.results ? commit('setFavoriteChannel',{data: 1, id: id}) : false;
            await dispatch('loadFavoriteChannels');
        },
        async removeFavoriteChannels({ commit,dispatch }, id) {
            const {data} = await ChannelsService.removeFavorite(id);
            data.results ? commit('setFavoriteChannel',{data: 0, id: id}) : false;
            await dispatch('loadFavoriteChannels');
        },
        async loadEPGLink({ commit }, id) {
            commit('startLoading','epg');
                commit('setStartTime', Date.now() );
                const {data} = await ChannelsService.epgLink(id);
                commit('setEPGLink', data.results);
            commit('stopLoading','epg');
        },
        async loadChannelsGenres({ commit }) {
            const {data} = await ChannelsService.genres();
            commit('setChannelsGenres', data.results);
        },

        // Films loadFilm
        async loadFilms({ commit }) {
            commit('startLoading','films');
                const {data} = await FilmsService.list();
                commit('setFilms',data.results);
                commit('set',{type: 'loadAllMovies', items: true});
            commit('stopLoading','films');
        },

        async loadFilmsAwait({ state, commit, dispatch }) {
            const {data} = await FilmsService.listAwait( state.films_ids.length);
            commit('setFilmsAwait',data.results);

            if (state.films_ids.length < state.filmsLength) {
                commit('set',{type: 'loadAllFilmsFlag', items: false});
                await dispatch('loadFilmsAwait');
            } else {
                commit('set',{type: 'loadAllFilmsFlag', items: true});
            }
        },
        async loadFilmsLength({ commit }) {
            commit('startLoading','filmsLength');
                const {data} = await FilmsService.listLength();
                commit('set',{type: 'filmsLength', items: data.results});
            commit('stopLoading','filmsLength');
        },
        async loadFilmsLast({ commit }) {
            commit('startLoading','films');
                const {data} = await FilmsService.listLast();
                commit('setFilmsLast',data.results);
                commit('set',{type: 'loadAllMovies', items: true});
            commit('stopLoading','films');
        },
        async loadFilmsTop({ commit }) {
            commit('startLoading','filmsTop');
                const {data} = await FilmsService.listTop();
                commit('setFilmsTop',data.results);
            commit('stopLoading','filmsTop');
        },
        async loadFilmsFavorite({ commit }) {
            commit('startLoading','filmsFavorite');
                const {data} = await FilmsService.listFavorite();
                commit('setFilmsFavorite',data.results);
            commit('stopLoading','filmsFavorite');
        },
        async loadFilmsTopToday({ commit }) {
            commit('startLoading','filmsTopToday');
                const {data} = await FilmsService.listTopToday();
                commit('setFilmsTopToday',data.results);
            commit('stopLoading','filmsTopToday');
        },
        async loadFilmsTopMonth({ commit }) {
            commit('startLoading','filmsTopMonth');
                const {data} = await FilmsService.listTopMonth();
                commit('setFilmsTopMonth',data.results);
            commit('stopLoading','filmsTopMonth');
        },
        async loadFilmInfo({ commit }, id) {
            commit('startLoading', 'film');
                const {data} = await FilmsService.view(id);
                commit('setFilm',data.results);
            commit('stopLoading', 'film');
        },
        async loadFilmMore({ commit }, id) {
            commit('startLoading', 'filmMore');
                const {data} = await FilmsService.viewMore(id);
                commit('setFilmMore',data.results[0]);
            commit('stopLoading', 'filmMore');
        },
        async loadFilmQuality({ commit }, id) {
            commit('startLoading', 'filmQuality');
                const {data} = await FilmsService.viewQuality(id);
                commit('setFilmQuality', {data: data.results, id: id});
            commit('stopLoading', 'filmQuality');
        },
        async loadFilmFrames({ commit }, id) {
            commit('startLoading', 'filmFrames');
                const {data} = await FilmsService.viewFrames(id);
                commit('setFilmFrames', {data: data.results, id: id});
            commit('stopLoading', 'filmFrames');
        },
        async loadFilmVotes({ commit }, id) {
            commit('startLoading', 'filmVotes');
                const {data} = await FilmsService.viewVotes(id);
                commit('setFilmVotes', {data: data.results[0], id: id});
            commit('stopLoading', 'filmVotes');
        },
        async likeFilmVotes({ commit,dispatch }, {id,like}) {
            commit('startLoading', 'filmVotesLike');
                const {data} = await FilmsService.likeVotes(id,like);
                await dispatch('loadFilmVotes', id);
            commit('stopLoading', 'filmVotesLike');
        },
        async loadFilmLink({ commit }, id) {
            commit('startLoading', 'link');
                const {data} = await FilmsService.link(id);
                commit('setFilmLink',{data: data.results, id: id});
            commit('stopLoading', 'link');
        },
        async commitSerialLink({ commit }, params) {
            commit('startLoading', 'link');
                commit('setFilmLink',params);
                setTimeout(function () {
                    commit('stopLoading', 'link');
                },1000)
        },
        async loadSerialLink({ commit }, params) {
            commit('startLoading', 'link');
                const {data} = await FilmsService.episodesLink(params.id, params.indexSeries);
                commit('setFilmLink',{data: data.results, id: params.id});
            commit('stopLoading', 'link');
        },
        async loadFilmsGenres({ commit }) {
            const {data} = await FilmsService.genres();
            commit('setFilmsGenres', data.results);
        },
        async notEnded({ commit }, {id,time}) {
            const {data} = await FilmsService.notEnded(id, time);
            commit('setNotEnded', {id, time});
        },
        async notEndedSeries({ commit }, {id,time, episode}) {
            const {data} = await FilmsService.notEndedSeries(id, time, episode);
            console.log('notEndedSeries', data);
            commit('setNotEndedEpisode', {id, time, episode});
        },
        async destroyNotEnded({ commit }, id) {
            const {data} = await FilmsService.destroyNotEndedTime(id);
            commit('destroyNotEnded', id);
        },
        async addFavoriteFilm({ commit, dispatch }, id) {
            const {data} = await FilmsService.addFavorite(id);
            data.results ? commit('setFavoriteFilm',{data: 1, id: id}) : false;

            await dispatch('loadFilmsFavorite');
        },
        async removeFavoriteFilm({ commit, dispatch }, id) {
            const {data} = await FilmsService.removeFavorite(id);
            data.results ? commit('setFavoriteFilm',{data: 0, id: id}) : false;

            await dispatch('loadFilmsFavorite');
        },
    }
})
