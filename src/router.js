import Vue from 'vue'
import Router from 'vue-router'

import BaseLayout from '@/views/layouts/BaseLayout'

import LoginPage from '@/views/LoginPage'
import MainPage  from '@/views/index'

import Settings from '@/views/settings'

import ChannelsIndex from '@/views/channels/index'
import ChannelsView from '@/views/channels/view'
import Timeshift from '@/views/channels/timeshift'

import FilmsIndex from '@/views/films/index'
import FilmsView from '@/views/films/view'

import CamerasIndex from '@/views/cameras/index'

Vue.use(Router);


const isLoggedIn = () => localStorage.getItem('token') !== "null";

export default new Router({
    mode: 'history',
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            to.meta.has_saved_scroll_pos = true;
            return savedPosition
        } else {
            to.meta.has_saved_scroll_pos = false;
            return {x: 0, y: 0}
        }
    },
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/login',
            name: 'Login',
            component: LoginPage,
            beforeEnter: (to, from, next) => {
                if (isLoggedIn()) { next({ path: '/' }) } else { next();}
            }
        },
        {
            path: '/logout',
            name: 'Logout',
            beforeEnter: (to, from, next) => {
                localStorage.setItem('token', null);
                localStorage.setItem('user', null);
                next({ path: '/login' })
            }
        },
        {
            path: '/',
            component: BaseLayout,
            children: [
                { path: '/', component: MainPage, name: 'MainPage' },

                { path: 'channels', component: ChannelsIndex, name: 'ChannelsIndex' },
                { path: 'channels/:id', component: ChannelsView, name: 'ChannelsView' },
                { path: 'channels/timeshift/:id', component: Timeshift, name: 'Timeshift' },
                { path: 'channels/category/:genreFilter', component: ChannelsIndex, name: 'ChannelsCategory' },

                { path: 'films', component: FilmsIndex, name: 'FilmsIndex' },
                { path: 'films/:id', component: FilmsView, name: 'FilmsView' },

                { path: 'cameras', component: CamerasIndex, name: 'CamerasIndex' },

                { path: 'settings', component: Settings, name: 'Settings' },
            ],
            beforeEnter: (to, from, next) => {
                if (isLoggedIn()) { next() } else { next({path: '/login'})  }
            }
        }

    ]
})
